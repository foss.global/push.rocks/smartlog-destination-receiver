/**
 * autocreated commitinfo by @pushrocks/commitinfo
 */
export const commitinfo = {
  name: '@pushrocks/smartlog-destination-receiver',
  version: '2.0.4',
  description: 'a smartlog destination for smartlog-receiver'
}
