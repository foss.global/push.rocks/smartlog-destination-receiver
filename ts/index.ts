import * as plugins from './smartlog-destination-receiver.plugins.js';
import {
  ILogDestination,
  ILogPackageAuthenticated,
  ILogPackage,
} from '@pushrocks/smartlog-interfaces';

export interface ISmartlogDestinationReceiverConstructorOptions {
  passphrase: string;
  receiverEndpoint: string;
}

export class SmartlogDestinationReceiver implements ILogDestination {
  private options: ISmartlogDestinationReceiverConstructorOptions;
  private webrequest = new plugins.webrequest.WebRequest();

  constructor(optionsArg: ISmartlogDestinationReceiverConstructorOptions) {
    this.options = optionsArg;
  }

  private errorCounter = 0;
  public async handleLog(logPackageArg: ILogPackage) {
    const response = await this.webrequest.postJson(this.options.receiverEndpoint, {
      auth: plugins.smarthash.sha256FromStringSync(this.options.passphrase),
      logPackage: logPackageArg,
    }).catch(err => {
      if (this.errorCounter % 100 === 0) {
        console.error(`There seems to be an error with logging.`);
        console.error(`Accumulated ${this.errorCounter} errors so far`)
      }
      this.errorCounter++;
    });
    return response.body;
  }
}
